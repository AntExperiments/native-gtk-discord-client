#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;
const WebKit2 = imports.gi.WebKit2
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray;

// Import Style
let styleSheet = ByteArray.toString(GLib.file_get_contents("./style.css")[1]);

Gtk.init(null);

const HeaderBarWindow = Lang.Class({
    Name: "HeaderBarWindow",
    Extends: Gtk.Window,


    _init: function() {
        this.parent({title: "HeaderBar Demo"});
        this.set_border_width(0);
        this.set_default_size(800, 400);

        let hb = new Gtk.HeaderBar();
        hb.set_show_close_button(true);
        hb.set_title("Discord");
        this.set_titlebar(hb);
        this.set_icon_name('fractal');


        // Buttons

        //#region Left Box
        let box = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL});

        // button = new Gtk.Button();
        // button.add(new Gtk.Image({ iconName: 'system-users-symbolic' }));
        // button.connect('clicked', () => webView.run_javascript("alert('test')", null, null));
        // box.add(button);

        button = new Gtk.Button();
        button.add(new Gtk.Image({ iconName: 'system-search-symbolic' }));
        button.connect('clicked', () => webView.run_javascript("document.querySelector('.searchBarComponent-32dTOx').click()", null, null));
        box.add(button);

        hb.pack_start(box);
        //#endregion Left Box

        //#region Right Box
        let rightBox = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL, spacing: 6});

        // button = new Gtk.Button();
        // button.add(new Gtk.Image({ iconName: 'call-start-symbolic' }));
        // button.connect('clicked', () => webView.run_javascript("document.querySelector('.toolbar-1t6TWx > .iconWrapper-2OrFZ1.clickable-3rdHwn:nth-child(1)').click()", null, null));
        // rightBox.add(button);

        // button = new Gtk.Button();
        // button.add(new Gtk.Image({ iconName: 'camera-web-symbolic' }));
        // button.connect('clicked', () => webView.run_javascript("document.querySelector('.toolbar-1t6TWx > .iconWrapper-2OrFZ1.clickable-3rdHwn:nth-child(2)').click()", null, null));
        // rightBox.add(button);

        button = new Gtk.Button();
        button.add(new Gtk.Image({ iconName: 'view-pin-symbolic' }));
        button.connect('clicked', () => webView.run_javascript("document.querySelector('.toolbar-1t6TWx > .iconWrapper-2OrFZ1.clickable-3rdHwn:nth-child(3)').click()", null, null));
        rightBox.add(button);
        button = new Gtk.Button();
        button.add(new Gtk.Image({ iconName: 'mail-inbox-symbolic' }));
        button.connect('clicked', () => webView.run_javascript("document.querySelector('.toolbar-1t6TWx > .iconWrapper-2OrFZ1.clickable-3rdHwn:nth-child(6)').click()", null, null));
        rightBox.add(button);

        hb.pack_end(rightBox);
        //#endregion Right Box

        // Container
        scrollWindow = new Gtk.ScrolledWindow({}),

        webView = new WebKit2.WebView(),
        webView.load_uri('https://discord.com/app');
        scrollWindow.add(webView);

        webView.connect('notify::title', async () => {
          // Hide Bar
          webView.run_javascript("document.querySelector('section.title-3qD0b-.container-1r6BKw.themed-ANHk51').style.height = '0px'", null, null);
          webView.run_javascript("document.querySelector('section.title-3qD0b-.container-1r6BKw.themed-ANHk51').style.overflow = 'hidden'", null, null);

          // Mute/Deaf buttons (don't work anyways)
          webView.run_javascript("document.querySelector('button.button-14-BFJ.enabled-2cQ-u7.button-38aScr.lookBlank-3eh9lL.colorBrand-3pXr91.grow-q77ONN:nth-child(1)').style.display = 'none'", null, null);
          webView.run_javascript("document.querySelector('button.button-14-BFJ.enabled-2cQ-u7.button-38aScr.lookBlank-3eh9lL.colorBrand-3pXr91.grow-q77ONN:nth-child(2)').style.display = 'none'", null, null);

          // Hide Search
          webView.run_javascript("document.querySelector('.searchBar-6Kv8R2').style.height = '0px'", null, null);
          webView.run_javascript("document.querySelector('.searchBar-6Kv8R2').style.overflow = 'hidden'", null, null);

          // Add stylesheet
          webView.run_javascript("var sheet = document.createElement('style')", null, null);
          webView.run_javascript(`sheet.innerHTML = \`${styleSheet}\``, null, null);
          webView.run_javascript("document.body.appendChild(sheet)", null, null);
        });

        this.add(scrollWindow);
    }
});

// Enable Dark mode
var gtk_settings = Gtk.Settings.get_default();
gtk_settings["gtk_application_prefer_dark_theme"] = true;

let win = new HeaderBarWindow();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
